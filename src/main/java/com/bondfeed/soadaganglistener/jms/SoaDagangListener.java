package com.bondfeed.soadaganglistener.jms;

import com.bondfeed.soadaganglistener.service.SoaDagangListenerService;
import com.bondfeed.soadaganglistener.service.impl.SoaDagangListenerServiceImpl;
import org.apache.activemq.Message;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;

@Component
public class SoaDagangListener {

//  @Qualifier("etp_db_config")
  @Autowired
  SoaDagangListenerService soaDagangListenerService;

  private static final Logger log = LoggerFactory.getLogger(SoaDagangListener.class);

//  private final OrderTransactionRepository transactionRepository;

//  public OrderTransactionReceiver(OrderTransactionRepository transactionRepository) {
//    this.transactionRepository = transactionRepository;
//  }

  private int count = 1;

  @JmsListener(id="soa-dagang-listener",destination = "queue.bondfeed.out", containerFactory = "myFactory")
  public void receiveMessage(Message message) {
    log.info("<" + count + "> Received <" + message + ">");

    if (message instanceof ActiveMQTextMessage) {
      ActiveMQTextMessage textMessage = (ActiveMQTextMessage) message;

      try {
        log.info("<" + count + "> Received <" + textMessage.getText() + ">");

        soaDagangListenerService.handleMessage(textMessage);
      } catch (Exception e) {
        log.error("ActiveMQTextMessage handling failed", e);
      }
    } else {
      log.error("Message is not a text message " + message.toString());
    }

    count++;
    //    throw new RuntimeException();
//    transactionRepository.save(transaction);
  }




}
