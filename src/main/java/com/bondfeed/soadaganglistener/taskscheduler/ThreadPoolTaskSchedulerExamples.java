package com.bondfeed.soadaganglistener.taskscheduler;

import java.util.Date;

import javax.annotation.PostConstruct;

import com.bondfeed.soadaganglistener.dao.etp_db.EtpFailoverLogDao;
import com.bondfeed.soadaganglistener.service.SoaDagangListenerService;
import com.bondfeed.soadaganglistener.thread.FixSPPACheckThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

@Component
public class ThreadPoolTaskSchedulerExamples {

    private static final Logger log = LoggerFactory.getLogger(FixSPPACheckThread.class);

    @Autowired
    private ThreadPoolTaskScheduler taskScheduler;

    @Autowired
    private SoaDagangListenerService soaDagangListenerService;

    @Autowired
    EtpFailoverLogDao etpFailoverLogDao;

    @Autowired
    Environment env;

    private Integer intervalOnThread = 30;
    private String jmsListenerSetting = "OFF";

    @PostConstruct
    public void scheduleRunnableWithCronTrigger() {

        taskScheduler.scheduleAtFixedRate(new RunnableTask("checkSPPAThreadRunning"), new Date(),intervalOnThread*1000);


    }

    class RunnableTask implements Runnable {

        private String message;

        public RunnableTask(String message) {
            this.message = message;
        }

        @Override
        public void run() {

            log.info("Runnable running every "+ intervalOnThread+"s");
            System.out.println("Thread checker running :" );
            readFromEnvProperties();

            soaDagangListenerService.updateSourceDataLog();

            if(!soaDagangListenerService.isSOAFailoverRunning()
                    &&soaDagangListenerService.isOverrideSOAFailoverTrue())
            {

                log.info("Start checking SPPA DATA dan SOA DATA :");

                soaDagangListenerService.checkMatchedDataThread(10);
            } else {
                log.info("System not triggering Failover due to Override SOA System Failover value is (OFF) or " +
                        "SOA System Failover status (ON)");

            }

        }

        public void readFromEnvProperties()
        {
            intervalOnThread = Integer.valueOf(env.getProperty("spring.interval.thread"));

        }
    }
}