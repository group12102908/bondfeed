package com.bondfeed.soadaganglistener.entity.etp_db;

import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "soa_order_log_message", schema = "public", catalog = "etp_db")
public class SoaOrderLogMessageEntity {
    @Basic
    @Column(name = "status")
    @SerializedName("Status")
    private String status;
    @Basic
    @Column(name = "f_i")
    @SerializedName("F/I")
    private String fI;
    @Basic
    @Column(name = "firm_code")
    @SerializedName("FirmCode")
    private String firmCode;
    @Basic
    @Column(name = "visible_size")
    @SerializedName("VisibleSize")
    private Long visibleSize;
    @Basic
    @Column(name = "size")
    @SerializedName("Size")
    private Long size;
    @Basic
    @Column(name = "output_time")
    @SerializedName("OutputTime")
    private String outputTime;
    @Basic
    @Column(name = "time")
    @SerializedName("Time")
    private String time;
    @Basic
    @Column(name = "order_id")
    @SerializedName("OrderID")
    private Long orderId;
    @Basic
    @Column(name = "workflow_id")
    @SerializedName("WorkflowID")
    private String workflowId;
    @Basic
    @Column(name = "valid_until")
    @SerializedName("ValidUntil")
    private String validUntil;
    @Basic
    @Column(name = "output_date")
    @SerializedName("OutputDate")
    private String outputDate;
    @Basic
    @Column(name = "settlement_date")
    @SerializedName("SettlementDate")
    private String settlementDate;
    @Basic
    @Column(name = "price")
    @SerializedName("Price")
    private String price;
    @Basic
    @Column(name = "time_in_force")
    @SerializedName("TimeInForce")
    private String timeInForce;
    @Basic
    @Column(name = "instrument")
    @SerializedName("Instrument")
    private String instrument;
    @Basic
    @Column(name = "yield")
    @SerializedName("Yield")
    private String yield;
    @Basic
    @Column(name = "firm_user_code")
    @SerializedName("FirmUserCode")
    private String firmUserCode;
    @Basic
    @Column(name = "created_date")
    @SerializedName("created_date")
    private Timestamp createdDate;
    @Basic
    @Column(name = "processed")
    @SerializedName("processed")
    private String processed;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "uid")
    private long uid;
    @Basic
    @Column(name = "order_id_daily")
    private long orderIdDaily;

    @Basic
    @Column(name = "side")
    @SerializedName("Side")
    private String side;


    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getfI() {
        return fI;
    }

    public void setfI(String fI) {
        this.fI = fI;
    }

    public String getFirmCode() {
        return firmCode;
    }

    public void setFirmCode(String firmCode) {
        this.firmCode = firmCode;
    }

    public Long getVisibleSize() {
        return visibleSize;
    }

    public void setVisibleSize(Long visibleSize) {
        this.visibleSize = visibleSize;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getOutputTime() {
        return outputTime;
    }

    public void setOutputTime(String outputTime) {
        this.outputTime = outputTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(String validUntil) {
        this.validUntil = validUntil;
    }

    public String getOutputDate() {
        return outputDate;
    }

    public void setOutputDate(String outputDate) {
        this.outputDate = outputDate;
    }

    public String getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTimeInForce() {
        return timeInForce;
    }

    public void setTimeInForce(String timeInForce) {
        this.timeInForce = timeInForce;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public String getYield() {
        return yield;
    }

    public void setYield(String yield) {
        this.yield = yield;
    }

    public String getFirmUserCode() {
        return firmUserCode;
    }

    public void setFirmUserCode(String firmUserCode) {
        this.firmUserCode = firmUserCode;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getProcessed() {
        return processed;
    }

    public void setProcessed(String processed) {
        this.processed = processed;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getOrderIdDaily() {
        return orderIdDaily;
    }

    public void setOrderIdDaily(long orderIdDaily) {
        this.orderIdDaily = orderIdDaily;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SoaOrderLogMessageEntity that = (SoaOrderLogMessageEntity) o;

        if (uid != that.uid) return false;
        if (orderIdDaily != that.orderIdDaily) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (fI != null ? !fI.equals(that.fI) : that.fI != null) return false;
        if (firmCode != null ? !firmCode.equals(that.firmCode) : that.firmCode != null) return false;
        if (visibleSize != null ? !visibleSize.equals(that.visibleSize) : that.visibleSize != null) return false;
        if (size != null ? !size.equals(that.size) : that.size != null) return false;
        if (outputTime != null ? !outputTime.equals(that.outputTime) : that.outputTime != null) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;
        if (orderId != null ? !orderId.equals(that.orderId) : that.orderId != null) return false;
        if (workflowId != null ? !workflowId.equals(that.workflowId) : that.workflowId != null) return false;
        if (validUntil != null ? !validUntil.equals(that.validUntil) : that.validUntil != null) return false;
        if (outputDate != null ? !outputDate.equals(that.outputDate) : that.outputDate != null) return false;
        if (settlementDate != null ? !settlementDate.equals(that.settlementDate) : that.settlementDate != null)
            return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (timeInForce != null ? !timeInForce.equals(that.timeInForce) : that.timeInForce != null) return false;
        if (instrument != null ? !instrument.equals(that.instrument) : that.instrument != null) return false;
        if (yield != null ? !yield.equals(that.yield) : that.yield != null) return false;
        if (firmUserCode != null ? !firmUserCode.equals(that.firmUserCode) : that.firmUserCode != null) return false;
        if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null) return false;
        if (processed != null ? !processed.equals(that.processed) : that.processed != null) return false;
        if (side != null ? !side.equals(that.side) : that.side != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = status != null ? status.hashCode() : 0;
        result = 31 * result + (fI != null ? fI.hashCode() : 0);
        result = 31 * result + (firmCode != null ? firmCode.hashCode() : 0);
        result = 31 * result + (visibleSize != null ? visibleSize.hashCode() : 0);
        result = 31 * result + (size != null ? size.hashCode() : 0);
        result = 31 * result + (outputTime != null ? outputTime.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (orderId != null ? orderId.hashCode() : 0);
        result = 31 * result + (workflowId != null ? workflowId.hashCode() : 0);
        result = 31 * result + (validUntil != null ? validUntil.hashCode() : 0);
        result = 31 * result + (outputDate != null ? outputDate.hashCode() : 0);
        result = 31 * result + (settlementDate != null ? settlementDate.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (timeInForce != null ? timeInForce.hashCode() : 0);
        result = 31 * result + (instrument != null ? instrument.hashCode() : 0);
        result = 31 * result + (yield != null ? yield.hashCode() : 0);
        result = 31 * result + (firmUserCode != null ? firmUserCode.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (processed != null ? processed.hashCode() : 0);
        result = 31 * result + (int) (uid ^ (uid >>> 32));
        result = 31 * result + (int) (orderIdDaily ^ (orderIdDaily >>> 32));
        result = 31 * result + (side != null ? side.hashCode() : 0);

        return result;
    }
}
