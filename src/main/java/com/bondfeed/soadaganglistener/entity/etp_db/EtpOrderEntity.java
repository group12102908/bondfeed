package com.bondfeed.soadaganglistener.entity.etp_db;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "etp_order", schema = "public", catalog = "etp_db")
public class EtpOrderEntity {
    @Basic
    @Column(name = "quote_id")
    private String quoteId;
    @Basic
    @Column(name = "board_id")
    private String boardId;
    @Basic
    @Column(name = "bond_id")
    private String bondId;
    @Basic
    @Column(name = "quotation_date")
    private Timestamp quotationDate;
    @Basic
    @Column(name = "expired_type")
    private String expiredType;
    @Basic
    @Column(name = "expired_time")
    private Date expiredTime;
    @Basic
    @Column(name = "dealer_from")
    private String dealerFrom;
    @Basic
    @Column(name = "investor_code")
    private String investorCode;
    @Basic
    @Column(name = "investor_firmid")
    private String investorFirmid;
    @Basic
    @Column(name = "dealer_from_ohoc")
    private String dealerFromOhoc;
    @Basic
    @Column(name = "dealer_to")
    private String dealerTo;
    @Basic
    @Column(name = "cp_investor_code")
    private String cpInvestorCode;
    @Basic
    @Column(name = "cp_investor_firmid")
    private String cpInvestorFirmid;
    @Basic
    @Column(name = "dealer_to_ohoc")
    private String dealerToOhoc;
    @Basic
    @Column(name = "bid_price")
    private BigDecimal bidPrice;
    @Basic
    @Column(name = "bid_yield")
    private BigDecimal bidYield;
    @Basic
    @Column(name = "bid_volume")
    private Integer bidVolume;
    @Basic
    @Column(name = "accrued_interest_bid")
    private BigDecimal accruedInterestBid;
    @Basic
    @Column(name = "total_cash_proceed_bid")
    private BigDecimal totalCashProceedBid;
    @Basic
    @Column(name = "offer_price")
    private BigDecimal offerPrice;
    @Basic
    @Column(name = "offer_yield")
    private BigDecimal offerYield;
    @Basic
    @Column(name = "offer_volume")
    private Integer offerVolume;
    @Basic
    @Column(name = "accrued_interest_offer")
    private BigDecimal accruedInterestOffer;
    @Basic
    @Column(name = "total_cash_proceed_offer")
    private BigDecimal totalCashProceedOffer;
    @Basic
    @Column(name = "settlement_date")
    private Date settlementDate;
    @Basic
    @Column(name = "prev_mid_price")
    private BigDecimal prevMidPrice;
    @Basic
    @Column(name = "type")
    private String type;
    @Basic
    @Column(name = "quote_type")
    private String quoteType;
    @Basic
    @Column(name = "remark")
    private String remark;
    @Basic
    @Column(name = "is_vas")
    private String isVas;
    @Basic
    @Column(name = "ref_quote_id")
    private Integer refQuoteId;
    @Basic
    @Column(name = "orig_quote_id")
    private Integer origQuoteId;
    @Basic
    @Column(name = "status")
    private String status;
    @Basic
    @Column(name = "created_by")
    private String createdBy;
    @Basic
    @Column(name = "created_date")
    private Date createdDate;
    @Basic
    @Column(name = "updated_by")
    private String updatedBy;
    @Basic
    @Column(name = "updated_date")
    private Date updatedDate;
    @Basic
    @Column(name = "cp_remark")
    private String cpRemark;
    @Basic
    @Column(name = "insert_by")
    private String insertBy;
    @Basic
    @Column(name = "insert_date")
    private Timestamp insertDate;
    @Basic
    @Column(name = "venue")
    private String venue;
    @Basic
    @Column(name = "gen_order_id")
    private Integer genOrderId;
    @Basic
    @Column(name = "source_data")
    private String sourceData;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "unique_id")
    private int uniqueId;

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public String getBondId() {
        return bondId;
    }

    public void setBondId(String bondId) {
        this.bondId = bondId;
    }

    public Timestamp getQuotationDate() {
        return quotationDate;
    }

    public void setQuotationDate(Timestamp quotationDate) {
        this.quotationDate = quotationDate;
    }

    public String getExpiredType() {
        return expiredType;
    }

    public void setExpiredType(String expiredType) {
        this.expiredType = expiredType;
    }

    public Date getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(Date expiredTime) {
        this.expiredTime = expiredTime;
    }

    public String getDealerFrom() {
        return dealerFrom;
    }

    public void setDealerFrom(String dealerFrom) {
        this.dealerFrom = dealerFrom;
    }

    public String getInvestorCode() {
        return investorCode;
    }

    public void setInvestorCode(String investorCode) {
        this.investorCode = investorCode;
    }

    public String getInvestorFirmid() {
        return investorFirmid;
    }

    public void setInvestorFirmid(String investorFirmid) {
        this.investorFirmid = investorFirmid;
    }

    public String getDealerFromOhoc() {
        return dealerFromOhoc;
    }

    public void setDealerFromOhoc(String dealerFromOhoc) {
        this.dealerFromOhoc = dealerFromOhoc;
    }

    public String getDealerTo() {
        return dealerTo;
    }

    public void setDealerTo(String dealerTo) {
        this.dealerTo = dealerTo;
    }

    public String getCpInvestorCode() {
        return cpInvestorCode;
    }

    public void setCpInvestorCode(String cpInvestorCode) {
        this.cpInvestorCode = cpInvestorCode;
    }

    public String getCpInvestorFirmid() {
        return cpInvestorFirmid;
    }

    public void setCpInvestorFirmid(String cpInvestorFirmid) {
        this.cpInvestorFirmid = cpInvestorFirmid;
    }

    public String getDealerToOhoc() {
        return dealerToOhoc;
    }

    public void setDealerToOhoc(String dealerToOhoc) {
        this.dealerToOhoc = dealerToOhoc;
    }

    public BigDecimal getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(BigDecimal bidPrice) {
        this.bidPrice = bidPrice;
    }

    public BigDecimal getBidYield() {
        return bidYield;
    }

    public void setBidYield(BigDecimal bidYield) {
        this.bidYield = bidYield;
    }

    public Integer getBidVolume() {
        return bidVolume;
    }

    public void setBidVolume(Integer bidVolume) {
        this.bidVolume = bidVolume;
    }

    public BigDecimal getAccruedInterestBid() {
        return accruedInterestBid;
    }

    public void setAccruedInterestBid(BigDecimal accruedInterestBid) {
        this.accruedInterestBid = accruedInterestBid;
    }

    public BigDecimal getTotalCashProceedBid() {
        return totalCashProceedBid;
    }

    public void setTotalCashProceedBid(BigDecimal totalCashProceedBid) {
        this.totalCashProceedBid = totalCashProceedBid;
    }

    public BigDecimal getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(BigDecimal offerPrice) {
        this.offerPrice = offerPrice;
    }

    public BigDecimal getOfferYield() {
        return offerYield;
    }

    public void setOfferYield(BigDecimal offerYield) {
        this.offerYield = offerYield;
    }

    public Integer getOfferVolume() {
        return offerVolume;
    }

    public void setOfferVolume(Integer offerVolume) {
        this.offerVolume = offerVolume;
    }

    public BigDecimal getAccruedInterestOffer() {
        return accruedInterestOffer;
    }

    public void setAccruedInterestOffer(BigDecimal accruedInterestOffer) {
        this.accruedInterestOffer = accruedInterestOffer;
    }

    public BigDecimal getTotalCashProceedOffer() {
        return totalCashProceedOffer;
    }

    public void setTotalCashProceedOffer(BigDecimal totalCashProceedOffer) {
        this.totalCashProceedOffer = totalCashProceedOffer;
    }

    public Date getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(Date settlementDate) {
        this.settlementDate = settlementDate;
    }

    public BigDecimal getPrevMidPrice() {
        return prevMidPrice;
    }

    public void setPrevMidPrice(BigDecimal prevMidPrice) {
        this.prevMidPrice = prevMidPrice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuoteType() {
        return quoteType;
    }

    public void setQuoteType(String quoteType) {
        this.quoteType = quoteType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIsVas() {
        return isVas;
    }

    public void setIsVas(String isVas) {
        this.isVas = isVas;
    }

    public Integer getRefQuoteId() {
        return refQuoteId;
    }

    public void setRefQuoteId(Integer refQuoteId) {
        this.refQuoteId = refQuoteId;
    }

    public Integer getOrigQuoteId() {
        return origQuoteId;
    }

    public void setOrigQuoteId(Integer origQuoteId) {
        this.origQuoteId = origQuoteId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCpRemark() {
        return cpRemark;
    }

    public void setCpRemark(String cpRemark) {
        this.cpRemark = cpRemark;
    }

    public String getInsertBy() {
        return insertBy;
    }

    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    public Timestamp getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Timestamp insertDate) {
        this.insertDate = insertDate;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public Integer getGenOrderId() {
        return genOrderId;
    }

    public void setGenOrderId(Integer genOrderId) {
        this.genOrderId = genOrderId;
    }

    public String getSourceData() {
        return sourceData;
    }

    public void setSourceData(String sourceData) {
        this.sourceData = sourceData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EtpOrderEntity that = (EtpOrderEntity) o;

        if (quoteId != null ? !quoteId.equals(that.quoteId) : that.quoteId != null) return false;
        if (boardId != null ? !boardId.equals(that.boardId) : that.boardId != null) return false;
        if (bondId != null ? !bondId.equals(that.bondId) : that.bondId != null) return false;
        if (quotationDate != null ? !quotationDate.equals(that.quotationDate) : that.quotationDate != null)
            return false;
        if (expiredType != null ? !expiredType.equals(that.expiredType) : that.expiredType != null) return false;
        if (expiredTime != null ? !expiredTime.equals(that.expiredTime) : that.expiredTime != null) return false;
        if (dealerFrom != null ? !dealerFrom.equals(that.dealerFrom) : that.dealerFrom != null) return false;
        if (investorCode != null ? !investorCode.equals(that.investorCode) : that.investorCode != null) return false;
        if (investorFirmid != null ? !investorFirmid.equals(that.investorFirmid) : that.investorFirmid != null)
            return false;
        if (dealerFromOhoc != null ? !dealerFromOhoc.equals(that.dealerFromOhoc) : that.dealerFromOhoc != null)
            return false;
        if (dealerTo != null ? !dealerTo.equals(that.dealerTo) : that.dealerTo != null) return false;
        if (cpInvestorCode != null ? !cpInvestorCode.equals(that.cpInvestorCode) : that.cpInvestorCode != null)
            return false;
        if (cpInvestorFirmid != null ? !cpInvestorFirmid.equals(that.cpInvestorFirmid) : that.cpInvestorFirmid != null)
            return false;
        if (dealerToOhoc != null ? !dealerToOhoc.equals(that.dealerToOhoc) : that.dealerToOhoc != null) return false;
        if (bidPrice != null ? !bidPrice.equals(that.bidPrice) : that.bidPrice != null) return false;
        if (bidYield != null ? !bidYield.equals(that.bidYield) : that.bidYield != null) return false;
        if (bidVolume != null ? !bidVolume.equals(that.bidVolume) : that.bidVolume != null) return false;
        if (accruedInterestBid != null ? !accruedInterestBid.equals(that.accruedInterestBid) : that.accruedInterestBid != null)
            return false;
        if (totalCashProceedBid != null ? !totalCashProceedBid.equals(that.totalCashProceedBid) : that.totalCashProceedBid != null)
            return false;
        if (offerPrice != null ? !offerPrice.equals(that.offerPrice) : that.offerPrice != null) return false;
        if (offerYield != null ? !offerYield.equals(that.offerYield) : that.offerYield != null) return false;
        if (offerVolume != null ? !offerVolume.equals(that.offerVolume) : that.offerVolume != null) return false;
        if (accruedInterestOffer != null ? !accruedInterestOffer.equals(that.accruedInterestOffer) : that.accruedInterestOffer != null)
            return false;
        if (totalCashProceedOffer != null ? !totalCashProceedOffer.equals(that.totalCashProceedOffer) : that.totalCashProceedOffer != null)
            return false;
        if (settlementDate != null ? !settlementDate.equals(that.settlementDate) : that.settlementDate != null)
            return false;
        if (prevMidPrice != null ? !prevMidPrice.equals(that.prevMidPrice) : that.prevMidPrice != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (quoteType != null ? !quoteType.equals(that.quoteType) : that.quoteType != null) return false;
        if (remark != null ? !remark.equals(that.remark) : that.remark != null) return false;
        if (isVas != null ? !isVas.equals(that.isVas) : that.isVas != null) return false;
        if (refQuoteId != null ? !refQuoteId.equals(that.refQuoteId) : that.refQuoteId != null) return false;
        if (origQuoteId != null ? !origQuoteId.equals(that.origQuoteId) : that.origQuoteId != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null) return false;
        if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;
        if (updatedDate != null ? !updatedDate.equals(that.updatedDate) : that.updatedDate != null) return false;
        if (cpRemark != null ? !cpRemark.equals(that.cpRemark) : that.cpRemark != null) return false;
        if (insertBy != null ? !insertBy.equals(that.insertBy) : that.insertBy != null) return false;
        if (insertDate != null ? !insertDate.equals(that.insertDate) : that.insertDate != null) return false;
        if (venue != null ? !venue.equals(that.venue) : that.venue != null) return false;
        if (genOrderId != null ? !genOrderId.equals(that.genOrderId) : that.genOrderId != null) return false;
        if (sourceData != null ? !sourceData.equals(that.sourceData) : that.sourceData != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = quoteId != null ? quoteId.hashCode() : 0;
        result = 31 * result + (boardId != null ? boardId.hashCode() : 0);
        result = 31 * result + (bondId != null ? bondId.hashCode() : 0);
        result = 31 * result + (quotationDate != null ? quotationDate.hashCode() : 0);
        result = 31 * result + (expiredType != null ? expiredType.hashCode() : 0);
        result = 31 * result + (expiredTime != null ? expiredTime.hashCode() : 0);
        result = 31 * result + (dealerFrom != null ? dealerFrom.hashCode() : 0);
        result = 31 * result + (investorCode != null ? investorCode.hashCode() : 0);
        result = 31 * result + (investorFirmid != null ? investorFirmid.hashCode() : 0);
        result = 31 * result + (dealerFromOhoc != null ? dealerFromOhoc.hashCode() : 0);
        result = 31 * result + (dealerTo != null ? dealerTo.hashCode() : 0);
        result = 31 * result + (cpInvestorCode != null ? cpInvestorCode.hashCode() : 0);
        result = 31 * result + (cpInvestorFirmid != null ? cpInvestorFirmid.hashCode() : 0);
        result = 31 * result + (dealerToOhoc != null ? dealerToOhoc.hashCode() : 0);
        result = 31 * result + (bidPrice != null ? bidPrice.hashCode() : 0);
        result = 31 * result + (bidYield != null ? bidYield.hashCode() : 0);
        result = 31 * result + (bidVolume != null ? bidVolume.hashCode() : 0);
        result = 31 * result + (accruedInterestBid != null ? accruedInterestBid.hashCode() : 0);
        result = 31 * result + (totalCashProceedBid != null ? totalCashProceedBid.hashCode() : 0);
        result = 31 * result + (offerPrice != null ? offerPrice.hashCode() : 0);
        result = 31 * result + (offerYield != null ? offerYield.hashCode() : 0);
        result = 31 * result + (offerVolume != null ? offerVolume.hashCode() : 0);
        result = 31 * result + (accruedInterestOffer != null ? accruedInterestOffer.hashCode() : 0);
        result = 31 * result + (totalCashProceedOffer != null ? totalCashProceedOffer.hashCode() : 0);
        result = 31 * result + (settlementDate != null ? settlementDate.hashCode() : 0);
        result = 31 * result + (prevMidPrice != null ? prevMidPrice.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (quoteType != null ? quoteType.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + (isVas != null ? isVas.hashCode() : 0);
        result = 31 * result + (refQuoteId != null ? refQuoteId.hashCode() : 0);
        result = 31 * result + (origQuoteId != null ? origQuoteId.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        result = 31 * result + (updatedDate != null ? updatedDate.hashCode() : 0);
        result = 31 * result + (cpRemark != null ? cpRemark.hashCode() : 0);
        result = 31 * result + (insertBy != null ? insertBy.hashCode() : 0);
        result = 31 * result + (insertDate != null ? insertDate.hashCode() : 0);
        result = 31 * result + (venue != null ? venue.hashCode() : 0);
        result = 31 * result + (genOrderId != null ? genOrderId.hashCode() : 0);
        result = 31 * result + (sourceData != null ? sourceData.hashCode() : 0);
        return result;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }
}
