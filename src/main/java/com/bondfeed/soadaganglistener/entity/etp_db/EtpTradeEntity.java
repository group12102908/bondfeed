package com.bondfeed.soadaganglistener.entity.etp_db;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "etp_trade", schema = "public", catalog = "etp_db")
public class EtpTradeEntity {
    @Basic
    @Column(name = "trade_id")
    private String tradeId;
    @Basic
    @Column(name = "plte_id")
    private String plteId;
    @Basic
    @Column(name = "quote_id")
    private String quoteId;
    @Basic
    @Column(name = "trade_date")
    private Timestamp tradeDate;
    @Basic
    @Column(name = "transaction_source")
    private String transactionSource;
    @Basic
    @Column(name = "transaction_type")
    private String transactionType;
    @Basic
    @Column(name = "trade_board")
    private String tradeBoard;
    @Basic
    @Column(name = "trade_report_type")
    private String tradeReportType;
    @Basic
    @Column(name = "settle_date")
    private Date settleDate;
    @Basic
    @Column(name = "bond_id")
    private String bondId;
    @Basic
    @Column(name = "price")
    private BigDecimal price;
    @Basic
    @Column(name = "yield")
    private BigDecimal yield;
    @Basic
    @Column(name = "volume")
    private Integer volume;
    @Basic
    @Column(name = "value")
    private BigDecimal value;
    @Basic
    @Column(name = "accrued_interest")
    private BigDecimal accruedInterest;
    @Basic
    @Column(name = "proceed")
    private BigDecimal proceed;
    @Basic
    @Column(name = "seller_firm_id")
    private String sellerFirmId;
    @Basic
    @Column(name = "seller_investor_code")
    private String sellerInvestorCode;
    @Basic
    @Column(name = "seller_account")
    private String sellerAccount;
    @Basic
    @Column(name = "seller_fd")
    private String sellerFd;
    @Basic
    @Column(name = "seller_type")
    private String sellerType;
    @Basic
    @Column(name = "seller_remarks")
    private String sellerRemarks;
    @Basic
    @Column(name = "buyer_firm_id")
    private String buyerFirmId;
    @Basic
    @Column(name = "buyer_investor_code")
    private String buyerInvestorCode;
    @Basic
    @Column(name = "buyer_account")
    private String buyerAccount;
    @Basic
    @Column(name = "buyer_fd")
    private String buyerFd;
    @Basic
    @Column(name = "buyer_type")
    private String buyerType;
    @Basic
    @Column(name = "buyer_remarks")
    private String buyerRemarks;
    @Basic
    @Column(name = "vas")
    private String vas;
    @Basic
    @Column(name = "action_name")
    private String actionName;
    @Basic
    @Column(name = "seller_login")
    private String sellerLogin;
    @Basic
    @Column(name = "buyer_login")
    private String buyerLogin;
    @Basic
    @Column(name = "insert_by")
    private String insertBy;
    @Basic
    @Column(name = "insert_date")
    private Timestamp insertDate;
    @Basic
    @Column(name = "venue")
    private String venue;
    @Basic
    @Column(name = "tradecommand")
    private String tradecommand;
    @Basic
    @Column(name = "gen_trade_id")
    private Integer genTradeId;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "unique_id")
    private int uniqueId;



    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getPlteId() {
        return plteId;
    }

    public void setPlteId(String plteId) {
        this.plteId = plteId;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public Timestamp getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(Timestamp tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getTransactionSource() {
        return transactionSource;
    }

    public void setTransactionSource(String transactionSource) {
        this.transactionSource = transactionSource;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTradeBoard() {
        return tradeBoard;
    }

    public void setTradeBoard(String tradeBoard) {
        this.tradeBoard = tradeBoard;
    }

    public String getTradeReportType() {
        return tradeReportType;
    }

    public void setTradeReportType(String tradeReportType) {
        this.tradeReportType = tradeReportType;
    }

    public Date getSettleDate() {
        return settleDate;
    }

    public void setSettleDate(Date settleDate) {
        this.settleDate = settleDate;
    }

    public String getBondId() {
        return bondId;
    }

    public void setBondId(String bondId) {
        this.bondId = bondId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getYield() {
        return yield;
    }

    public void setYield(BigDecimal yield) {
        this.yield = yield;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getAccruedInterest() {
        return accruedInterest;
    }

    public void setAccruedInterest(BigDecimal accruedInterest) {
        this.accruedInterest = accruedInterest;
    }

    public BigDecimal getProceed() {
        return proceed;
    }

    public void setProceed(BigDecimal proceed) {
        this.proceed = proceed;
    }

    public String getSellerFirmId() {
        return sellerFirmId;
    }

    public void setSellerFirmId(String sellerFirmId) {
        this.sellerFirmId = sellerFirmId;
    }

    public String getSellerInvestorCode() {
        return sellerInvestorCode;
    }

    public void setSellerInvestorCode(String sellerInvestorCode) {
        this.sellerInvestorCode = sellerInvestorCode;
    }

    public String getSellerAccount() {
        return sellerAccount;
    }

    public void setSellerAccount(String sellerAccount) {
        this.sellerAccount = sellerAccount;
    }

    public String getSellerFd() {
        return sellerFd;
    }

    public void setSellerFd(String sellerFd) {
        this.sellerFd = sellerFd;
    }

    public String getSellerType() {
        return sellerType;
    }

    public void setSellerType(String sellerType) {
        this.sellerType = sellerType;
    }

    public String getSellerRemarks() {
        return sellerRemarks;
    }

    public void setSellerRemarks(String sellerRemarks) {
        this.sellerRemarks = sellerRemarks;
    }

    public String getBuyerFirmId() {
        return buyerFirmId;
    }

    public void setBuyerFirmId(String buyerFirmId) {
        this.buyerFirmId = buyerFirmId;
    }

    public String getBuyerInvestorCode() {
        return buyerInvestorCode;
    }

    public void setBuyerInvestorCode(String buyerInvestorCode) {
        this.buyerInvestorCode = buyerInvestorCode;
    }

    public String getBuyerAccount() {
        return buyerAccount;
    }

    public void setBuyerAccount(String buyerAccount) {
        this.buyerAccount = buyerAccount;
    }

    public String getBuyerFd() {
        return buyerFd;
    }

    public void setBuyerFd(String buyerFd) {
        this.buyerFd = buyerFd;
    }

    public String getBuyerType() {
        return buyerType;
    }

    public void setBuyerType(String buyerType) {
        this.buyerType = buyerType;
    }

    public String getBuyerRemarks() {
        return buyerRemarks;
    }

    public void setBuyerRemarks(String buyerRemarks) {
        this.buyerRemarks = buyerRemarks;
    }

    public String getVas() {
        return vas;
    }

    public void setVas(String vas) {
        this.vas = vas;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getSellerLogin() {
        return sellerLogin;
    }

    public void setSellerLogin(String sellerLogin) {
        this.sellerLogin = sellerLogin;
    }

    public String getBuyerLogin() {
        return buyerLogin;
    }

    public void setBuyerLogin(String buyerLogin) {
        this.buyerLogin = buyerLogin;
    }

    public String getInsertBy() {
        return insertBy;
    }

    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    public Timestamp getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Timestamp insertDate) {
        this.insertDate = insertDate;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getTradecommand() {
        return tradecommand;
    }

    public void setTradecommand(String tradecommand) {
        this.tradecommand = tradecommand;
    }

    public Integer getGenTradeId() {
        return genTradeId;
    }

    public void setGenTradeId(Integer genTradeId) {
        this.genTradeId = genTradeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EtpTradeEntity that = (EtpTradeEntity) o;

        if (tradeId != null ? !tradeId.equals(that.tradeId) : that.tradeId != null) return false;
        if (plteId != null ? !plteId.equals(that.plteId) : that.plteId != null) return false;
        if (quoteId != null ? !quoteId.equals(that.quoteId) : that.quoteId != null) return false;
        if (tradeDate != null ? !tradeDate.equals(that.tradeDate) : that.tradeDate != null) return false;
        if (transactionSource != null ? !transactionSource.equals(that.transactionSource) : that.transactionSource != null)
            return false;
        if (transactionType != null ? !transactionType.equals(that.transactionType) : that.transactionType != null)
            return false;
        if (tradeBoard != null ? !tradeBoard.equals(that.tradeBoard) : that.tradeBoard != null) return false;
        if (tradeReportType != null ? !tradeReportType.equals(that.tradeReportType) : that.tradeReportType != null)
            return false;
        if (settleDate != null ? !settleDate.equals(that.settleDate) : that.settleDate != null) return false;
        if (bondId != null ? !bondId.equals(that.bondId) : that.bondId != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (yield != null ? !yield.equals(that.yield) : that.yield != null) return false;
        if (volume != null ? !volume.equals(that.volume) : that.volume != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (accruedInterest != null ? !accruedInterest.equals(that.accruedInterest) : that.accruedInterest != null)
            return false;
        if (proceed != null ? !proceed.equals(that.proceed) : that.proceed != null) return false;
        if (sellerFirmId != null ? !sellerFirmId.equals(that.sellerFirmId) : that.sellerFirmId != null) return false;
        if (sellerInvestorCode != null ? !sellerInvestorCode.equals(that.sellerInvestorCode) : that.sellerInvestorCode != null)
            return false;
        if (sellerAccount != null ? !sellerAccount.equals(that.sellerAccount) : that.sellerAccount != null)
            return false;
        if (sellerFd != null ? !sellerFd.equals(that.sellerFd) : that.sellerFd != null) return false;
        if (sellerType != null ? !sellerType.equals(that.sellerType) : that.sellerType != null) return false;
        if (sellerRemarks != null ? !sellerRemarks.equals(that.sellerRemarks) : that.sellerRemarks != null)
            return false;
        if (buyerFirmId != null ? !buyerFirmId.equals(that.buyerFirmId) : that.buyerFirmId != null) return false;
        if (buyerInvestorCode != null ? !buyerInvestorCode.equals(that.buyerInvestorCode) : that.buyerInvestorCode != null)
            return false;
        if (buyerAccount != null ? !buyerAccount.equals(that.buyerAccount) : that.buyerAccount != null) return false;
        if (buyerFd != null ? !buyerFd.equals(that.buyerFd) : that.buyerFd != null) return false;
        if (buyerType != null ? !buyerType.equals(that.buyerType) : that.buyerType != null) return false;
        if (buyerRemarks != null ? !buyerRemarks.equals(that.buyerRemarks) : that.buyerRemarks != null) return false;
        if (vas != null ? !vas.equals(that.vas) : that.vas != null) return false;
        if (actionName != null ? !actionName.equals(that.actionName) : that.actionName != null) return false;
        if (sellerLogin != null ? !sellerLogin.equals(that.sellerLogin) : that.sellerLogin != null) return false;
        if (buyerLogin != null ? !buyerLogin.equals(that.buyerLogin) : that.buyerLogin != null) return false;
        if (insertBy != null ? !insertBy.equals(that.insertBy) : that.insertBy != null) return false;
        if (insertDate != null ? !insertDate.equals(that.insertDate) : that.insertDate != null) return false;
        if (venue != null ? !venue.equals(that.venue) : that.venue != null) return false;
        if (tradecommand != null ? !tradecommand.equals(that.tradecommand) : that.tradecommand != null) return false;
        if (genTradeId != null ? !genTradeId.equals(that.genTradeId) : that.genTradeId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tradeId != null ? tradeId.hashCode() : 0;
        result = 31 * result + (plteId != null ? plteId.hashCode() : 0);
        result = 31 * result + (quoteId != null ? quoteId.hashCode() : 0);
        result = 31 * result + (tradeDate != null ? tradeDate.hashCode() : 0);
        result = 31 * result + (transactionSource != null ? transactionSource.hashCode() : 0);
        result = 31 * result + (transactionType != null ? transactionType.hashCode() : 0);
        result = 31 * result + (tradeBoard != null ? tradeBoard.hashCode() : 0);
        result = 31 * result + (tradeReportType != null ? tradeReportType.hashCode() : 0);
        result = 31 * result + (settleDate != null ? settleDate.hashCode() : 0);
        result = 31 * result + (bondId != null ? bondId.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (yield != null ? yield.hashCode() : 0);
        result = 31 * result + (volume != null ? volume.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (accruedInterest != null ? accruedInterest.hashCode() : 0);
        result = 31 * result + (proceed != null ? proceed.hashCode() : 0);
        result = 31 * result + (sellerFirmId != null ? sellerFirmId.hashCode() : 0);
        result = 31 * result + (sellerInvestorCode != null ? sellerInvestorCode.hashCode() : 0);
        result = 31 * result + (sellerAccount != null ? sellerAccount.hashCode() : 0);
        result = 31 * result + (sellerFd != null ? sellerFd.hashCode() : 0);
        result = 31 * result + (sellerType != null ? sellerType.hashCode() : 0);
        result = 31 * result + (sellerRemarks != null ? sellerRemarks.hashCode() : 0);
        result = 31 * result + (buyerFirmId != null ? buyerFirmId.hashCode() : 0);
        result = 31 * result + (buyerInvestorCode != null ? buyerInvestorCode.hashCode() : 0);
        result = 31 * result + (buyerAccount != null ? buyerAccount.hashCode() : 0);
        result = 31 * result + (buyerFd != null ? buyerFd.hashCode() : 0);
        result = 31 * result + (buyerType != null ? buyerType.hashCode() : 0);
        result = 31 * result + (buyerRemarks != null ? buyerRemarks.hashCode() : 0);
        result = 31 * result + (vas != null ? vas.hashCode() : 0);
        result = 31 * result + (actionName != null ? actionName.hashCode() : 0);
        result = 31 * result + (sellerLogin != null ? sellerLogin.hashCode() : 0);
        result = 31 * result + (buyerLogin != null ? buyerLogin.hashCode() : 0);
        result = 31 * result + (insertBy != null ? insertBy.hashCode() : 0);
        result = 31 * result + (insertDate != null ? insertDate.hashCode() : 0);
        result = 31 * result + (venue != null ? venue.hashCode() : 0);
        result = 31 * result + (tradecommand != null ? tradecommand.hashCode() : 0);
        result = 31 * result + (genTradeId != null ? genTradeId.hashCode() : 0);
        return result;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }
}
