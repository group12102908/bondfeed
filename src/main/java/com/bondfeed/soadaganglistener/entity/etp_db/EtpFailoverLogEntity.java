package com.bondfeed.soadaganglistener.entity.etp_db;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "etp_failover_log", schema = "public", catalog = "etp_db")
public class EtpFailoverLogEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;
    @Basic
    @Column(name = "status")
    private String status;
    @Basic
    @Column(name = "timestamp_log")
    private Timestamp timestampLog;
    @Basic
    @Column(name = "description")
    private String description;

    @Basic
    @Column(name = "type")
    private String type;

    public EtpFailoverLogEntity() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getTimestampLog() {
        return timestampLog;
    }

    public void setTimestampLog(Timestamp timestampLog) {
        this.timestampLog = timestampLog;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EtpFailoverLogEntity that = (EtpFailoverLogEntity) o;

        if (id != that.id) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (timestampLog != null ? !timestampLog.equals(that.timestampLog) : that.timestampLog != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (timestampLog != null ? timestampLog.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (type !=null ? type.hashCode() : 0);
        return result;
    }

    public EtpFailoverLogEntity( String status, String description, String type) {
        this.status = status;
        this.description = description;
        this.type = type;
        java.sql.Date newSqlDate = new java.sql.Date(new Date().getTime());

        this.timestampLog = new Timestamp(newSqlDate.getTime());
    }
}
