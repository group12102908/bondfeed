package com.bondfeed.soadaganglistener.entity.bondfeed_db;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "\"TSourceDataLog\"", schema = "public", catalog = "bondfeeddb_dev")
public class TSourceDataLogEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id_datasource", nullable = false)
    private int idDatasource;
    @Basic
    @Column(name = "datasource", nullable = true, length = 20)
    private String datasource;
    @Basic
    @Column(name = "status_datasource", nullable = true, length = 20)
    private String statusDatasource;
    @Basic
    @Column(name = "total_data", nullable = true)
    private Integer totalData;
    @Basic
    @Column(name = "lastdata_log", nullable = true)
    private Timestamp lastdataLog;

    public int getIdDatasource() {
        return idDatasource;
    }

    public void setIdDatasource(int idDatasource) {
        this.idDatasource = idDatasource;
    }

    public String getDatasource() {
        return datasource;
    }

    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    public String getStatusDatasource() {
        return statusDatasource;
    }

    public void setStatusDatasource(String statusDatasource) {
        this.statusDatasource = statusDatasource;
    }

    public Integer getTotalData() {
        return totalData;
    }

    public void setTotalData(Integer totalData) {
        this.totalData = totalData;
    }

    public Timestamp getLastdataLog() {
        return lastdataLog;
    }

    public void setLastdataLog(Timestamp lastdataLog) {
        this.lastdataLog = lastdataLog;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TSourceDataLogEntity that = (TSourceDataLogEntity) o;

        if (idDatasource != that.idDatasource) return false;
        if (datasource != null ? !datasource.equals(that.datasource) : that.datasource != null) return false;
        if (statusDatasource != null ? !statusDatasource.equals(that.statusDatasource) : that.statusDatasource != null)
            return false;
        if (totalData != null ? !totalData.equals(that.totalData) : that.totalData != null) return false;
        if (lastdataLog != null ? !lastdataLog.equals(that.lastdataLog) : that.lastdataLog != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idDatasource;
        result = 31 * result + (datasource != null ? datasource.hashCode() : 0);
        result = 31 * result + (statusDatasource != null ? statusDatasource.hashCode() : 0);
        result = 31 * result + (totalData != null ? totalData.hashCode() : 0);
        result = 31 * result + (lastdataLog != null ? lastdataLog.hashCode() : 0);
        return result;
    }
}
