package com.bondfeed.soadaganglistener.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
//@PropertySource({"classpath:persistence-multiple-db-boot.properties"})
@EnableJpaRepositories(basePackages = "com.bondfeed.soadaganglistener.dao.bondfeed_db", entityManagerFactoryRef = "bondfeedEntityManager", transactionManagerRef = "bondfeedTransactionManager")
@Profile("!tc")
public class BondfeedDbConfig {
    @Autowired
    private Environment env;

    public BondfeedDbConfig() {
        super();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean bondfeedEntityManager() {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(bondfeedDataSource());
        em.setPackagesToScan("com.bondfeed.soadaganglistener.entity.bondfeed_db");

        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        final HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));

        properties.put("hibernate.connection.url", env.getProperty("spring.second-datasource.url"));
        properties.put("hibernate.connection.username", env.getProperty("spring.second-datasource.username"));
        properties.put("hibernate.connection.password", env.getProperty("spring.second-datasource.password"));
        properties.put("hibernate.globally_quoted_identifiers", env.getProperty("spring.second-datasource.globally_quoted_identifiers"));

        properties.put("spring.jpa.hibernate.naming_strategy",
                "org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl");
//        spring.jpa.hibernate.naming.physical-strategy=org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl


        em.setJpaPropertyMap(properties);

        return em;
    }

    @Bean
    @ConfigurationProperties(prefix="spring.second-datasource")
    public DataSource bondfeedDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public PlatformTransactionManager bondfeedTransactionManager() {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(bondfeedEntityManager().getObject());
        return transactionManager;
    }

}