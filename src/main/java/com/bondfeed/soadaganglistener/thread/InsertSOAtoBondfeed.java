package com.bondfeed.soadaganglistener.thread;

import com.bondfeed.soadaganglistener.dao.etp_db.EtpFailoverLogDao;
import com.bondfeed.soadaganglistener.dao.etp_db.EtpOrderDao;
import com.bondfeed.soadaganglistener.entity.etp_db.EtpFailoverLogEntity;
import com.bondfeed.soadaganglistener.service.SoaDagangListenerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InsertSOAtoBondfeed implements Runnable{

    private static final Logger log = LoggerFactory.getLogger(InsertSOAtoBondfeed.class);

    @Autowired
    SoaDagangListenerService soaDagangListenerService;

    @Autowired
    EtpOrderDao etpOrderDao;

    @Autowired
    EtpFailoverLogDao etpFailoverLogDao;

    public void run()
    {
//        String latestLogMessageOrderId = soaDagangListenerService.findByLatestLogMessageOrderId();
//
//        List<SoaOrderLogMessageEntity> soaOrderLogMessageEntityList = soaDagangListenerService.findByOrderByUidAsc();
//        SoaOrderLogMessageEntity soaOrderLogMessageEntity = null;
//        if(!soaOrderLogMessageEntityList.isEmpty())
//        {
//            soaOrderLogMessageEntity = soaOrderLogMessageEntityList.get(0);
//        }
////         soaOrderLogMessageEntity = soaDagangListenerService.findByOrderByUidAsc();
//
//        String latestSoaOrderId = "";
//
//        if(soaOrderLogMessageEntity != null)
//        {
//            latestSoaOrderId = soaOrderLogMessageEntity.getOrderId().toString()
//                    .concat(soaOrderLogMessageEntity.getSide().equalsIgnoreCase("ASK")?"_1":"_0");
//        }
//
        String latestEtpOrderId = soaDagangListenerService.findByLatestEtpOrder().getQuoteId();

//        if(latestEtpOrderId.equalsIgnoreCase(latestSoaOrderId)&&
//                latestEtpOrderId.equalsIgnoreCase(latestLogMessageOrderId))
//        {
//            log.info("Latest etpOrder already match!");
//
//
//            EtpFailoverLogEntity findLatestLog = etpFailoverLogDao.findFirstByTypeOrderByTimestampLogAsc("SOA_FAILOVER_SYSTEM");
//            if(findLatestLog.getStatus().equalsIgnoreCase("ON"))
//            {
//                etpFailoverLogDao.save(new EtpFailoverLogEntity(
//                        "OFF","Latest etpOrder already match!","SOA_FAILOVER_SYSTEM"
//                ));
//            }
//
//
//
//        } else {
            log.info("SOA System Failover Turned ON");
            EtpFailoverLogEntity newEtpFailoverLog =
                    new EtpFailoverLogEntity("ON","SOA System Failover Turned ON","SOA_FAILOVER_SYSTEM");

            etpFailoverLogDao.save(newEtpFailoverLog);

            soaDagangListenerService.insertSOAtoBonfeedThread(latestEtpOrderId);
//        }

    }
}
