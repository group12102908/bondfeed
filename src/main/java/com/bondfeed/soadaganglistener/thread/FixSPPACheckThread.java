package com.bondfeed.soadaganglistener.thread;

import com.bondfeed.soadaganglistener.dao.etp_db.LogMessageDao;
import com.bondfeed.soadaganglistener.dao.etp_db.SoaOrderLogMessageDao;
import com.bondfeed.soadaganglistener.service.SoaDagangListenerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class FixSPPACheckThread implements Runnable{
    private static final Logger log = LoggerFactory.getLogger(FixSPPACheckThread.class);


    @Autowired
    private ApplicationContext applicationContext;


    @Autowired
    SoaOrderLogMessageDao soaOrderLogMessageDao;

    @Autowired
    LogMessageDao logMessageDao;

    @Autowired
    SoaDagangListenerService soaDagangListenerService;


    public void run()
    {

        log.info("Runnable running every 5s");
        System.out.println("Hello runnable");
        System.out.println("Thread checker running :" );

        if(!soaDagangListenerService.isSOAFailoverRunning()
                &&soaDagangListenerService.isOverrideSOAFailoverTrue())
        {

            log.info("Start checking SPPA DATA dan SOA DATA :");

            soaDagangListenerService.checkMatchedDataThread(5);
        } else {
            log.info("System not triggering Failover due to Override SOA System Failover value is (OFF) or " +
                    "SOA System Failover status (ON)");

        }

//        Runnable helloRunnable = new Runnable() {
//            public void run() {
//
////                FixSPPACheckThread checkThread = new FixSPPACheckThread();
////
////                checkThread.start();
//            }
//
//        };
//        applicationContext.getAutowireCapableBeanFactory().autowireBean(helloRunnable);


//        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
//        executor.scheduleAtFixedRate(helloRunnable, 0, 5, TimeUnit.SECONDS);

    }


}
