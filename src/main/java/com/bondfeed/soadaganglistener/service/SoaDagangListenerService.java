package com.bondfeed.soadaganglistener.service;

import com.bondfeed.soadaganglistener.entity.etp_db.EtpOrderEntity;
import com.bondfeed.soadaganglistener.entity.etp_db.SoaOrderLogMessageEntity;
import org.apache.activemq.command.ActiveMQTextMessage;

import java.util.List;

public interface SoaDagangListenerService {
    void stopSoaOrderListener();

    void startSoaOrderListener();

    boolean isRunningSoaOrderListener();

    @Deprecated
    SoaOrderLogMessageEntity convertMessagetoEntity(String message) throws Exception;
    void handleMessage(ActiveMQTextMessage message) throws Exception;

    boolean isSOAFailoverRunning();

    boolean isOverrideSOAFailoverTrue();


    void checkMatchedDataThread(Integer limit);

    void insertSOAtoBonfeedThread (String startOrderId);

    String findByLatestLogMessageOrderId();

    EtpOrderEntity findByLatestEtpOrder();

    List<SoaOrderLogMessageEntity> findByOrderByUidAsc();

    void updateSourceDataLog();



//    void updateSourceDataLog();
}
