package com.bondfeed.soadaganglistener.service.impl;

import com.bondfeed.soadaganglistener.constant.Constant;
import com.bondfeed.soadaganglistener.dao.bondfeed_db.TSourceDataLogDao;
import com.bondfeed.soadaganglistener.dao.etp_db.EtpFailoverLogDao;
import com.bondfeed.soadaganglistener.dao.etp_db.EtpOrderDao;
import com.bondfeed.soadaganglistener.dao.etp_db.LogMessageDao;
import com.bondfeed.soadaganglistener.dao.etp_db.SoaOrderLogMessageDao;
import com.bondfeed.soadaganglistener.entity.bondfeed_db.TSourceDataLogEntity;
import com.bondfeed.soadaganglistener.entity.etp_db.EtpFailoverLogEntity;
import com.bondfeed.soadaganglistener.entity.etp_db.EtpOrderEntity;
import com.bondfeed.soadaganglistener.entity.etp_db.LogMessageEntity;
import com.bondfeed.soadaganglistener.entity.etp_db.SoaOrderLogMessageEntity;
import com.bondfeed.soadaganglistener.service.SoaDagangListenerService;
import com.bondfeed.soadaganglistener.thread.InsertSOAtoBondfeed;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Example;
import org.springframework.jms.config.JmsListenerEndpointRegistry;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.*;

@Service
public class SoaDagangListenerServiceImpl implements SoaDagangListenerService {

    private static final Logger log = LoggerFactory.getLogger(SoaDagangListenerServiceImpl.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    SoaOrderLogMessageDao soaOrderLogMessageDao;

    @Autowired
    LogMessageDao logMessageDao;

    @Autowired
    EtpFailoverLogDao etpFailoverLogDao;

    @Autowired
    EtpOrderDao etpOrderDao;

    @Autowired
    TSourceDataLogDao tSourceDataLogDao;

    @Autowired
    private ThreadPoolTaskScheduler taskScheduler;

    @Autowired
    JmsListenerEndpointRegistry registry;

//    @Autowired
//    public YourService(JmsListenerEndpointRegistry registry) {
//        this.registry = registry;
//    }

    @Override
    public void stopSoaOrderListener() {
        registry.getListenerContainer("soa-dagang-listener").stop();
    }

    @Override
    public void startSoaOrderListener() {
        registry.getListenerContainer("soa-dagang-listener").start();
    }

    @Override
    public boolean isRunningSoaOrderListener() {
        return registry.getListenerContainer("soa-dagang-listener").isRunning();
    }



    @Override
    @Deprecated
    public SoaOrderLogMessageEntity convertMessagetoEntity(String message) throws Exception
    {
        try {
//            String jsonString = message; //assign your JSON String here

            Gson gson = new Gson();

            JsonObject jsonObject = new JsonParser().parse(message).getAsJsonObject();

            SoaOrderLogMessageEntity logMessage = gson.fromJson(jsonObject.get("raw").toString(),
                    SoaOrderLogMessageEntity.class);

            System.out.println(logMessage.getStatus()); //Status

            System.out.println("test output");
            System.out.println(logMessage);
//        System.out.println(gson.toJson(person)); // {"name":"John"}

            logMessage.setProcessed("NO");
            logMessage.setCreatedDate(new Timestamp(new Date().getTime()));
            return logMessage;
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return  null;
        }
    }

    @Override
    public void handleMessage(ActiveMQTextMessage message) throws Exception {
        try {
            String json = message.getText();
            SoaOrderLogMessageEntity newEntity = convertMessagetoEntity(json);
            Example<SoaOrderLogMessageEntity> example = Example.of(newEntity);

            List<SoaOrderLogMessageEntity> listSoaOrderDaily =
                    soaOrderLogMessageDao.findAllByToday();
            Integer orderIdDaily = 0;
            if(!listSoaOrderDaily.isEmpty())
            {
                orderIdDaily = Math.toIntExact(listSoaOrderDaily.get(0).getOrderIdDaily()) + 1;
            } else {
                orderIdDaily = 1;
            }

            newEntity.setOrderIdDaily(orderIdDaily);

            soaOrderLogMessageDao.save(newEntity);

//            if(soaOrderLogMessageDao.exists(example))
//            {
//                soaOrderLogMessageDao.save(newEntity);
//            } else {
////                log.info();
//                log.info("Data already exist, system will skip redundant data!"+newEntity.toString());
//            }
//      DoSomethingWithJSON(json);
        } catch (Exception e) {
            System.out.println("Could not extract data to log from TextMessage");
            throw e;
        }

    }

    @Override
    public boolean isSOAFailoverRunning() {

        boolean isFailoverActive = etpFailoverLogDao.findFirstByTypeOrderByTimestampLogAsc("SOA_FAILOVER_SYSTEM").getStatus().equalsIgnoreCase("ON");

        return isFailoverActive;
    }

    @Override
    public boolean isOverrideSOAFailoverTrue() {

        boolean isOverrideSOAFailoverTrue = etpFailoverLogDao.findFirstByTypeOrderByTimestampLogAsc(Constant.OVERRIDE_FAILOVER_SYSTEM).getStatus().equalsIgnoreCase("ON");

        return isOverrideSOAFailoverTrue;
    }

    public String parseFixGetOrderId(String fixData)
    {

        Map<String, Object> orderDataMap = new HashMap();

        String[] data_split = fixData.split("\001");
        List<String> wordList = Arrays.asList(data_split);
        for (String valueSplit : wordList) {
            String[] value_split = valueSplit.split("\\=");
            orderDataMap.put(value_split[0].trim(), value_split[1].trim());
        }
        String sppa_time_output = orderDataMap.containsKey("273") ? orderDataMap.get("273").toString() : "";
        String order_id =orderDataMap.containsKey("278") ? orderDataMap.get("278").toString() : "";
//        return  new Double(order_id.replace("_1","").replace("_0",""));
        return  order_id;

    }

    public LocalDateTime parseFixGetDate(String fixData)
    {

        Map<String, Object> orderDataMap = new HashMap();

        String[] data_split = fixData.split("\001");
        List<String> wordList = Arrays.asList(data_split);
        for (String valueSplit : wordList) {
            String[] value_split = valueSplit.split("\\=");
            orderDataMap.put(value_split[0].trim(), value_split[1].trim());
        }
        String sppa_date_output = orderDataMap.containsKey("272") ? orderDataMap.get("272").toString() : "";

        String sppa_time_output = orderDataMap.containsKey("273") ? orderDataMap.get("273").toString() : "";
        String order_id =orderDataMap.containsKey("278") ? orderDataMap.get("278").toString() : "";

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate date = LocalDate.parse(sppa_date_output, dateFormatter);

        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime time = LocalTime.parse(sppa_time_output, timeFormatter);
//        time.plusHours(7);

        LocalDateTime combineDateTime = LocalDateTime.of(date, time);



        return  combineDateTime;

    }

    public void checkMatchedDataThread(Integer limit)
    {
        Integer dataMatched = 0;
        List<SoaOrderLogMessageEntity> soaOrderLogMessageEntityList =
                soaOrderLogMessageDao.findAllByTodayByProcessedAndLimitDesc("NO", limit);

        List<LogMessageEntity> logMessageEntityList =
                logMessageDao.findAllByTodayByTypeAndLimitDesc("X",limit);

        List<String> listMatchOrderId = new ArrayList<>();
        List<String[]> listUnmatchedOrderId = new ArrayList<>();

        List<LocalDateTime> listSPPAFixDate = new ArrayList<>();

        List<LocalDateTime> listSOADate = new ArrayList<>();

        if(soaOrderLogMessageEntityList.size()>0)
        {
            for(SoaOrderLogMessageEntity itemSoaOrder : soaOrderLogMessageEntityList)
            {
                DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String dateItem = itemSoaOrder.getOutputDate();
                LocalDate date = LocalDate.parse(dateItem, dateFormatter);

                DateTimeFormatter timeFormatter = new DateTimeFormatterBuilder()
                        .appendPattern("HH:mm:ss")
                        .appendFraction(ChronoField.MILLI_OF_SECOND, 1, 9, true) // min 2 max 3
                        .toFormatter();


//            DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSSSSS");
                String timeItem = itemSoaOrder.getOutputTime();
                LocalTime time = LocalTime.parse(timeItem, timeFormatter);
                time.plusHours(7);

                LocalDateTime combineDateTime = LocalDateTime.of(date, time);

                if(!listSOADate.contains(combineDateTime))
                {
                    listSOADate.add(combineDateTime);
                }
                for(LogMessageEntity itemLogMessage : logMessageEntityList) {

                    LocalDateTime logMessageDate = parseFixGetDate(itemLogMessage.getMessage());

                    if(!listSPPAFixDate.contains(logMessageDate))
                    {
                        listSPPAFixDate.add(logMessageDate);
                    }

                    String soaLogOrderId = itemSoaOrder.getOrderId().toString()
                            .concat(itemSoaOrder.getSide().equalsIgnoreCase("ASK")?"_1":"_0");
                    String logMessageOrderId = parseFixGetOrderId(itemLogMessage.getMessage());
                    if(soaLogOrderId.equalsIgnoreCase(logMessageOrderId))
                    {

                        if(!listMatchOrderId.contains(soaLogOrderId))
                        {
                            listMatchOrderId.add(soaLogOrderId);
                        }
                    }else
                    {
                        String[] arrayString = {soaLogOrderId,logMessageOrderId};
                        if(!listUnmatchedOrderId.contains(arrayString))
                        {
                            listUnmatchedOrderId.add(arrayString);
                        }
                    }
                }
            }


            LocalDateTime maxSoaDate = Collections.max(listSOADate);
            LocalDateTime maxSPPAFixDate = Collections.max(listSPPAFixDate);

            boolean sppaNewerThanSoa = maxSPPAFixDate.isAfter(maxSoaDate);


            dataMatched = listMatchOrderId.size();

            boolean isFailoverActive = etpFailoverLogDao.findFirstByTypeOrderByTimestampLogAsc("SOA_FAILOVER_SYSTEM").getStatus().equalsIgnoreCase("ON");


            boolean isLatestOrderMatch = false;
            //cek latest order Id
            String latestLogMessageOrderId2 = findByLatestLogMessageOrderId();

            List<SoaOrderLogMessageEntity> soaOrderLogMessageEntityList2 = findByOrderByUidAsc();
            SoaOrderLogMessageEntity soaOrderLogMessageEntity2 = null;
            if(!soaOrderLogMessageEntityList2.isEmpty())
            {
                soaOrderLogMessageEntity2 = soaOrderLogMessageEntityList.get(0);
            }
//         soaOrderLogMessageEntity = soaDagangListenerService.findByOrderByUidAsc();

            String latestSoaOrderId2 = "";

            if(soaOrderLogMessageEntity2 != null)
            {
                latestSoaOrderId2 = soaOrderLogMessageEntity2.getOrderId().toString()
                        .concat(soaOrderLogMessageEntity2.getSide().equalsIgnoreCase("ASK")?"_1":"_0");
            }

            String latestEtpOrderId = findByLatestEtpOrder().getQuoteId();

            if(latestEtpOrderId.equalsIgnoreCase(latestSoaOrderId2)&&
                    latestEtpOrderId.equalsIgnoreCase(latestLogMessageOrderId2))
            {
                isLatestOrderMatch =true;
            }

//            String latestEtpOrderId = findByLatestEtpOrder().getQuoteId();

            Double startOrderIdDouble = new Double(latestEtpOrderId.replace("_0","")
                    .replace("_1",""));

            List<SoaOrderLogMessageEntity> soaOrderLogMessageEntityListRec =
                    soaOrderLogMessageDao.findAllByTodayByProcessedAndLimitAndOrderIdDesc
                            ("NO",1,
                                    BigInteger.valueOf(startOrderIdDouble.longValue()) );

//            if(soaOrderLogMessageEntityListRec.size()!=0)

            if(dataMatched< (limit* 9/10) && !isFailoverActive
                    && !sppaNewerThanSoa && !isLatestOrderMatch
                    && !soaOrderLogMessageEntityListRec.isEmpty())
            {
            etpFailoverLogDao.save(new EtpFailoverLogEntity(
                    "ON","SOA System Failover is running!","SOA_FAILOVER_SYSTEM"
            ));


                //start new thread

                log.info("Starting to insert SOA to ETP Order");

                InsertSOAtoBondfeed runnableInsertService = new InsertSOAtoBondfeed();
                applicationContext.getAutowireCapableBeanFactory().autowireBean(runnableInsertService);

                taskScheduler.execute(runnableInsertService);

                //start new thread
            } else {
                log.info("System unable to proceed due to current status :");
                log.info("Data Matched : ".concat(dataMatched.toString()));
                log.info("Data Matched Threshold : ".concat(String.valueOf(limit* 9/10)));
                log.info("Logic 1, ".concat("Data Matched < Data Matched Threshold = ").concat(dataMatched< (limit* 9/10)?"TRUE":"FALSE"));
                log.info("Logic 2, ".concat("Is Failover Not Active =").concat(!isFailoverActive?"TRUE":"FALSE"));
                log.info("Logic 3, ".concat("Is SPPA Data Not Newer Than SOA Order Data = ").concat(!sppaNewerThanSoa?"TRUE":"FALSE"));
                log.info("Logic 4, ".concat("Is Latest Order Data Unmatched = ").concat(!isLatestOrderMatch?"TRUE":"FALSE"));
                log.info("Logic 5, ".concat("Is Unprocessed SOA Order Data Available = ").concat(!soaOrderLogMessageEntityListRec.isEmpty()?"TRUE":"FALSE"));



            }
        } else {
            log.info("SOA System Failover cannot run due to Unavailable Unprocessed SOA Order Data!");
        }




    }

//    public List<SoaOrderLogMessageEntity>

    public EtpOrderEntity soaOrderEntitytoEtpOrderEntity(SoaOrderLogMessageEntity item)
    {
        String order_type = item.getSide();

        EtpOrderEntity newEntity = new EtpOrderEntity();
        newEntity.setQuoteId(String.valueOf(item.getOrderId())
                .concat(order_type.equalsIgnoreCase("ASK")?"_1":"_0"));
        newEntity.setBondId(item.getInstrument());

        //quotation date

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String dateItem = item.getOutputDate();
        LocalDate date = LocalDate.parse(dateItem, dateFormatter);

        DateTimeFormatter timeFormatter = new DateTimeFormatterBuilder()
                .appendPattern("HH:mm:ss")
                .appendFraction(ChronoField.MILLI_OF_SECOND, 1, 9, true) // min 2 max 3
                .toFormatter();

//        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSSSSS");
        String timeItem = item.getOutputTime();
        LocalTime time = LocalTime.parse(timeItem, timeFormatter);
        time.plusHours(7);

        LocalDateTime combineDateTime = LocalDateTime.of(date, time);

        newEntity.setQuotationDate(Timestamp.valueOf(combineDateTime));

        //quotation date

//                    newEntity
        if(item.getSide().equalsIgnoreCase("ASK"))
        {
            newEntity.setOfferPrice(BigDecimal.valueOf(new Double(item.getPrice())));

            Double itemVolume = Double.valueOf(item.getSize()) / new Double(1000000);
            newEntity.setOfferVolume(itemVolume.intValue());

            newEntity.setOfferYield(BigDecimal.ZERO);
        } else
        {
            newEntity.setBidPrice(BigDecimal.valueOf(new Double(item.getPrice())));

            Double itemVolume = Double.valueOf(item.getSize()) / new Double(1000000);
            newEntity.setBidVolume(itemVolume.intValue());

            newEntity.setBidYield(BigDecimal.ZERO);
        }

        newEntity.setQuoteType(item.getfI().equalsIgnoreCase("F")?"1":"0");

        List<SoaOrderLogMessageEntity> relatedOrderSOA =
                soaOrderLogMessageDao.findRelatedOrderByWorkflowIdOrder(
                        item.getWorkflowId());

        String status = "";
//        if(relatedOrderSOA.size()==1)
//        {
//            status = "Open";
//        } else if(item.getStatus().equalsIgnoreCase("Cancelled"))
//        {
//            status = "Withdrawn";
//        }
//
//        else if(relatedOrderSOA.get(0).equals(item))
//        {
//            status = "Open";
//        } else
//        {
//            status = "Amended";
//        }
        if(relatedOrderSOA.size()==0)
        {
            status = "Open";
        } else if(item.getStatus().equalsIgnoreCase("Cancelled"))
        {
            status = "Withdrawn";
        } else
        {
            status = "Amended";
        }

        newEntity.setStatus(status);
        newEntity.setCreatedBy("SOA_FAILOVER_SYSTEM");

        java.sql.Date newSqlDate = new java.sql.Date(new Date().getTime());
        newEntity.setCreatedDate(newSqlDate);
        newEntity.setUpdatedDate(newSqlDate);

        newEntity.setInsertBy("SYSTEM");
        newEntity.setInsertDate(new Timestamp(newSqlDate.getTime()));
        newEntity.setSourceData("SOA_DAGANG");


        EtpOrderEntity latestEntity = etpOrderDao.findByLatestGenOrderId();
        newEntity.setGenOrderId(latestEntity.getGenOrderId()+1);

        return newEntity;




    }


    public void insertSOAtoBonfeedThread (String startOrderId)
    {
        boolean isFailoverActive = etpFailoverLogDao.findFirstByTypeOrderByTimestampLogAsc("SOA_FAILOVER_SYSTEM").getStatus().equalsIgnoreCase("ON");

        Double startOrderIdDouble = new Double(startOrderId.replace("_0","")
                .replace("_1",""));

        String side = startOrderId.contains("_1")?"ASK":"BID";
        if(isFailoverActive)
        {
            //recursive - done
            List<SoaOrderLogMessageEntity> soaOrderLogMessageEntityListRec =
                    soaOrderLogMessageDao.findAllByTodayByProcessedAndLimitAndOrderIdDesc
                            ("NO",1,
                                    BigInteger.valueOf(startOrderIdDouble.longValue()) );

            if(soaOrderLogMessageEntityListRec.size()!=0)
            {


                try{

                    SoaOrderLogMessageEntity insertData =
                            soaOrderLogMessageEntityListRec.get(0);

                    insertData.setProcessed("YES");
                    soaOrderLogMessageDao.save(insertData);

                    etpOrderDao.save(soaOrderEntitytoEtpOrderEntity(insertData));

                } catch (Exception error)
                {
                    soaOrderLogMessageEntityListRec.clear();
                    log.error(error.getMessage());

                    log.info("System SOA Failover Error - ".concat(error.getMessage()));
                    EtpFailoverLogEntity newEtpFailoverLog =
                            new EtpFailoverLogEntity("OFF","-","SOA_FAILOVER_SYSTEM");

                    etpFailoverLogDao.save(newEtpFailoverLog);

                    EtpFailoverLogEntity newEtpFailoverLogOverride =
                            new EtpFailoverLogEntity("OFF"," System SOA Failover Error reset system to off - Please Check Message"
                                    .concat(error.getMessage()), Constant.OVERRIDE_FAILOVER_SYSTEM);

                    etpFailoverLogDao.save(newEtpFailoverLogOverride);


                }
                insertSOAtoBonfeedThread(startOrderId);

            } else {
                log.info("Data processed currently depleted");
                EtpFailoverLogEntity newEtpFailoverLog =
                        new EtpFailoverLogEntity("OFF","-","SOA_FAILOVER_SYSTEM");

                etpFailoverLogDao.save(newEtpFailoverLog);
            }

//            //batch - not done
//            List<SoaOrderLogMessageEntity> soaOrderLogMessageEntityList =
//                    soaOrderLogMessageDao.findAllByTodayByProcessedAndLimit
//                            ("NO",1000, "ASC");
//
//            for(SoaOrderLogMessageEntity item : soaOrderLogMessageEntityList)
//            {
//                if(new Double(item.getOrderId())>startOrderId)
//                {
//                    String order_type = item.getSide();
//
//                    EtpOrderEntity newEntity = new EtpOrderEntity();
//                    newEntity.setQuoteId(String.valueOf(item.getOrderId())
//                            .concat(order_type.equalsIgnoreCase("ASK")?"_1":"_0"));
//                    newEntity.setBondId(item.getInstrument());
//
//                    //quotation date
//
//                    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//                    String dateItem = item.getSettlementDate();
//                    LocalDate date = LocalDate.parse(dateItem, dateFormatter);
//
//                    DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//                    String timeItem = item.getSettlementDate();
//                    LocalTime time = LocalTime.parse(dateItem, dateFormatter);
//                    time.plusHours(7);
//
//                    LocalDateTime combineDateTime = LocalDateTime.of(date, time);
//
//                    newEntity.setQuotationDate(Timestamp.valueOf(combineDateTime));
//
////                    newEntity
//                    if(item.getSide().equalsIgnoreCase("ASK"))
//                    {
//                        newEntity.setOfferPrice(BigDecimal.valueOf(new Double(item.getPrice())));
//
//                        Double itemVolume = Double.valueOf(item.getSize()) / new Double(1000000);
//                        newEntity.setOfferVolume(itemVolume.intValue());
//
////                        newEntity.setOfferYield();
//                    } else
//                    {
////                        newEntity.setBidPrice();
//                    }
//
//                    //quotation date
//
//
////                    etpOrderDao.save()
//                }
//            }
//
        }
    }

    @Override
    public String findByLatestLogMessageOrderId() {
        List<LogMessageEntity> logMessageEntityList =
        logMessageDao.findAllByTodayByTypeAndLimitAsc("X",1);

        if(logMessageEntityList.size()!=0)
        {

            LogMessageEntity findLogMessageEntity =
                    logMessageEntityList.get(0);

            return parseFixGetOrderId(findLogMessageEntity.getMessage());
        }
        return null;
    }

    @Override
    public EtpOrderEntity findByLatestEtpOrder() {

        return etpOrderDao.findByOrderByUniqueIdAsc();

    }

    @Override
    public List<SoaOrderLogMessageEntity> findByOrderByUidAsc() {
        return soaOrderLogMessageDao.findByOrderByUidAsc();
    }

    @Override
    public void updateSourceDataLog ()
    {
        TSourceDataLogEntity updateEntity = tSourceDataLogDao.findByDatasource("SOA ORDER");

        updateEntity.setTotalData(etpOrderDao.findAllBySourceDataToday("SOA_DAGANG").size());

        boolean isOverrideSOAFailoverTrue = etpFailoverLogDao.findFirstByTypeOrderByTimestampLogAsc(Constant.OVERRIDE_FAILOVER_SYSTEM).getStatus().equalsIgnoreCase("ON");
        updateEntity.setStatusDatasource(isOverrideSOAFailoverTrue?"ON":"OFF");

        updateEntity.setLastdataLog(new Timestamp(new Date().getTime()));

        tSourceDataLogDao.save(updateEntity);
    }

}
