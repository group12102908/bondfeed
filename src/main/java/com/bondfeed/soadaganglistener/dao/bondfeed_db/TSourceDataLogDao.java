package com.bondfeed.soadaganglistener.dao.bondfeed_db;

import com.bondfeed.soadaganglistener.entity.bondfeed_db.TSourceDataLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TSourceDataLogDao extends JpaRepository<TSourceDataLogEntity, Object> {

    @Query( value = "select * from \"TSourceDataLog\" srclog " +
            " where srclog.datasource = :dataSource " +
            " limit 1 ", nativeQuery = true)
    TSourceDataLogEntity findByDatasource(@Param("dataSource")String dataSource);
}
