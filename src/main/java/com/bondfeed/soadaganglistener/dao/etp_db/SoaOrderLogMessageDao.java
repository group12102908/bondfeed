package com.bondfeed.soadaganglistener.dao.etp_db;

import com.bondfeed.soadaganglistener.entity.etp_db.SoaOrderLogMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface SoaOrderLogMessageDao extends JpaRepository<SoaOrderLogMessageEntity, Object> {

    @Query(value = "select * from soa_order_log_message solm " +
            "where cast(solm.created_date as date)  = CURRENT_DATE and " +
            "solm.processed = :processed " +
            "ORDER BY solm.uid DESC LIMIT :limit ",nativeQuery = true)
    List<SoaOrderLogMessageEntity> findAllByTodayByProcessedAndLimitDesc
            (@Param("processed") String processed,@Param("limit") Integer limit);

    @Query(value = "select * from soa_order_log_message solm " +
            "where cast(solm.created_date as date)  = CURRENT_DATE  " +
            "and solm.order_id > :startOrderId " +
            "and solm.processed = :processed " +
//            "and solm.side = :side " +
            "ORDER BY uid ASC LIMIT :limit",nativeQuery = true)
    List<SoaOrderLogMessageEntity> findAllByTodayByProcessedAndLimitAndOrderIdDesc
            (@Param("processed")String processed,
             @Param("limit")Integer limit,
             @Param("startOrderId") BigInteger startOrderId);
    @Query(value = "select * from soa_order_log_message solm \n" +
            "where \n" +
            "solm.workflow_id = :workflowId \n" +
            "order by solm.uid ASC ", nativeQuery = true)
    List<SoaOrderLogMessageEntity> findRelatedOrderByWorkflowIdOrder(@Param("workflowId")String workflowId);


    @Query( value = "select * from soa_order_log_message solm \n" +

            "order by solm.uid DESC LIMIT 1", nativeQuery = true)
    List<SoaOrderLogMessageEntity> findByOrderByUidAsc();

    @Query( value = " select * from soa_order_log_message solm \n" +
            " where cast(solm.created_date as date)  = CURRENT_DATE " +
            " order by solm.uid DESC  " ,
                nativeQuery = true)
    List<SoaOrderLogMessageEntity> findAllByToday();


}
