package com.bondfeed.soadaganglistener.dao.etp_db;

import com.bondfeed.soadaganglistener.entity.etp_db.EtpFailoverLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EtpFailoverLogDao extends JpaRepository<EtpFailoverLogEntity, Object> {


    @Query(value = "select * from\n" +
            "\tetp_failover_log efl\n" +
            "where\n" +
            "\tefl.\"type\" = :type \n" +
            "order by\n" +
            "\ttimestamp_log desc\n" +
            "limit 1",nativeQuery = true)
    EtpFailoverLogEntity findFirstByTypeOrderByTimestampLogAsc(String type);


}
