package com.bondfeed.soadaganglistener.dao.etp_db;

import com.bondfeed.soadaganglistener.entity.etp_db.LogMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogMessageDao extends JpaRepository<LogMessageEntity, Object> {

    @Query(value = "select * from log_message lm " +
            "where cast(lm.timestamp as date)  = CURRENT_DATE and " +
            "lm.message_type = :type " +
            "order by lm.unique_id DESC LIMIT :limit",nativeQuery = true)
    List<LogMessageEntity> findAllByTodayByTypeAndLimitDesc(@Param("type")String type,@Param("limit") Integer limit);

    @Query(value = "select * from log_message lm " +
            "where cast(lm.timestamp as date)  = CURRENT_DATE and " +
            "lm.message_type = :type " +
            "order by lm.unique_id DESC LIMIT :limit",nativeQuery = true)
    List<LogMessageEntity> findAllByTodayByTypeAndLimitAsc(@Param("type")String type, @Param("limit")Integer limit);


}
