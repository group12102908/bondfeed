package com.bondfeed.soadaganglistener.dao.etp_db;

import com.bondfeed.soadaganglistener.entity.etp_db.EtpOrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EtpOrderDao extends JpaRepository<EtpOrderEntity, Object> {

    @Query( value = "select * from etp_order etp \n" +

            "order by etp.unique_id DESC LIMIT 1", nativeQuery = true)
    EtpOrderEntity findByOrderByUniqueIdAsc();


    @Query( value = "select * from etp_order " +
            "order by insert_date " +
            "desc limit 1" , nativeQuery = true)
    EtpOrderEntity findByLatestGenOrderId();

    @Query( value = " select * from etp_order etp " +
            " where  cast(etp.insert_date as date)  = CURRENT_DATE " +
            " and etp.source_data = :sourceData ", nativeQuery = true )
    List<EtpOrderEntity> findAllBySourceDataToday(@Param("sourceData")String sourceData);


}
